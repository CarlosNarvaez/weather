package com.example.weatherapp



data class WeatherResponse(var weather: List<Weather>?){}

data class Weather
    (
    var main: String?,  //si pongo el mismo nombre del jasson aqui, solo pongo y ya, pero si no pongo lo de arriba y pongo el nombre q quiera aqui
    var description:String?,
    var icon: String?){}
