package com.example.weatherapp


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_main_weather.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MainWeatherFragment : Fragment() {
    internal lateinit var cityName:TextView
    internal lateinit var weatherName: TextView
    internal lateinit var searchButton: Button
    internal lateinit var weatherImage: ImageView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cityName= view.findViewById(R.id.city_text_field)
        weatherName=view.findViewById(R.id.weather_response)
        searchButton= view.findViewById(R.id.button_search)
        searchButton.setOnClickListener { getWeather() }

        weatherImage= view.findViewById(R.id.imageView)

    }

    fun getWeather(){
        val weatherService= WeatherService.instance

        val weatherCall= weatherService.getWeather("2.5",cityName.text.toString())


        weatherCall.enqueue(object : Callback<WeatherResponse>{
            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                Log.d("ERROR", call.request().toString())
            }

            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                //val weatherList= response.body() as WeatherResponse
                Log.d("BODY", response.body().toString())
                val weatherList= response.body() as WeatherResponse
                val firstweather= weatherList.weather!!.get(0)
                weatherName.text= firstweather?.main?:"Error"


                val iconUrl=
                        "https://api.openweathermap.org/img/w/${firstweather?.icon?: "10d"}.png"

                Glide.with(context!!)
                    .load(iconUrl)
                    .into(imageView)
            }


        })
    }






    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_weather, container, false)
    }


}

