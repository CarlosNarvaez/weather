package com.example.weatherapp


import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WeatherService {
    @GET("/data/{api_version}/weather")
    fun getWeather(
        @Path("api_version") version: String="2.5",
        @Query("q") city:String,
        @Query("appid") appId:String= "774000082909cda8740e97f9f7baac86" // obtenemos de la cuenta que nos creamos en operweather
    ) : Call<WeatherResponse>


    companion object {
        val instance: WeatherService by lazy {
            val retrofit= Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }
}

